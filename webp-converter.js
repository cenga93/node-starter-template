import fs from 'fs';
import path from 'path';
import fg from 'fast-glob';
import webpConverter from 'webp-converter';

(() => {
     const pathToProject = './public/images/';
     const hasFolder = fs.existsSync(pathToProject);

     if (!hasFolder) {
          console.error(`Error! Directory ${pathToProject} does not exist.`);
          return;
     }

     const files = fg.sync([`${pathToProject}**/*.{png,jpg,jpeg}`]);

     if (!files.length) {
          return;
     }

     for (const fullFilePath of files) {
          const fileName = path.basename(fullFilePath, path.extname(fullFilePath));
          const filePath = path.dirname(fullFilePath);

          webpConverter.cwebp(fullFilePath, `${filePath}/${fileName}.webp`, '-q 90').then(() => {
               console.log(`Done webp conversion for: ${fullFilePath}`);
          });
     }
})();
