import fs from 'fs';
import gulp from 'gulp';
import sourcemaps from 'gulp-sourcemaps';
import babelify from 'babelify';
import dartSass from 'sass';
import browserSync from 'browser-sync';
import uglify from 'gulp-uglify';
import rename from 'gulp-rename';
import size from 'gulp-size';
import plumber from 'gulp-plumber';
import sassLint from 'gulp-sass-lint';
import gulpSass from 'gulp-sass';
import browserify from 'browserify';
import source from 'vinyl-source-stream';
import buffer from 'vinyl-buffer';
import sassvg from 'gulp-sassvg';
import nodemon from 'gulp-nodemon';

const sass = gulpSass(dartSass);
const timestamp = new Date().getTime();
const isProd = process.env.NODE_ENV === 'production';

const server = browserSync.create();

/** ****************************************
 ***               REUSABLE FUNCTIONS              ***
 ***************************************** */

/**
 *
 * @param src - This should be the .scss files for build
 * @param cb -  This should be the callback function
 */
const styleCompiler = ({ src, cb }) => {
     gulp.src(src)
          .pipe(sourcemaps.init())
          .pipe(plumber())
          .pipe(sassLint({ configFile: '.sass-lint.yml' }))
          .pipe(sassLint.format())
          .pipe(sass.sync({ outputStyle: 'compressed' }))
          .on('error', sass.logError)
          .pipe(rename({ extname: isProd ? `.min.${timestamp}.css` : '.min.css' }))
          .pipe(sourcemaps.write('./'))
          .pipe(gulp.dest('./public/css/'))
          .pipe(server.stream());

     cb();
};

/**
 *
 * @param src - This should be the .js files for build
 * @param cb -  This should be the callback function
 */
const javascriptCompiler = ({ JSFiles, cb }) => {
     const JSFolder = 'src/assets/js/';

     JSFiles.map((entry) =>
          browserify({ entries: [JSFolder + entry] }, { allowEmpty: true })
               .transform(babelify, { presets: ['@babel/env'] })
               .bundle()
               .pipe(source(entry))
               .pipe(rename({ extname: isProd ? `.min.${timestamp}.js` : '.min.js' }))
               .pipe(buffer())
               .pipe(sourcemaps.init({ loadMaps: true }))
               .pipe(uglify())
               .pipe(size())
               .pipe(sourcemaps.write('./'))
               .pipe(gulp.dest('./public/js'))
     );

     cb();
};

/** *************************
 ***               TASKS               ****
 ************************** */
gulp.task('criticalJS', (cb) => {
     const JSFiles = ['critical.js'];

     javascriptCompiler({ JSFiles, cb });
});

gulp.task('nonCriticalJS', (cb) => {
     const JSFiles = ['main.js'];

     javascriptCompiler({ JSFiles, cb });
});

gulp.task('nonCriticalCSS', (cb) => {
     const cssSRC = ['./src/assets/scss/main.scss'];

     styleCompiler({ src: cssSRC, cb });
});

gulp.task('criticalCSS', (cb) => {
     const cssSRC = ['./src/assets/scss/critical.scss'];

     styleCompiler({ src: cssSRC, cb });
});

gulp.task('svg', () => {
     const rootPath = './src/assets/svg';
     const svgSRC = [`${rootPath}/*.svg`];
     const pathToFolder = `${rootPath}/sassvg`;

     const hasFolder = fs.existsSync(pathToFolder);

     if (!hasFolder) {
          fs.mkdirSync(pathToFolder);
     }

     return gulp
          .src(svgSRC)
          .pipe(plumber())
          .pipe(sassvg({ outputFolder: pathToFolder }))
          .pipe(browserSync.stream());
});

gulp.task('serve', (cb) => {
     server.init(null, {
          proxy: 'localhost:3000',
          open: false,
          files: ['src/**/*.*'],
          port: 4000,
     });
     cb();
});

gulp.task('nodeDev', (done) => {
     let stream = nodemon({
          script: 'app.local.js',
          ignore: ['gulpfile.js', 'node_modules/', 'public/'],
          done: done,
     });
     stream
          .on('restart', function () {
               console.log('restarted!');
          })
          .on('crash', function () {
               console.error('Application has crashed!\n');
               stream.emit('restart', 10);
          });
});

gulp.task('watchTask', (cb) => {
     const tasks = ['criticalCSS', 'criticalJS', 'nonCriticalCSS', 'nonCriticalJS'];

     gulp.watch(['src/assets/scss/**/*.scss'], gulp.series(tasks));
     gulp.watch(['src/assets/js/**/*.js'], gulp.series(tasks));

     cb();
});

gulp.task('default', gulp.series('criticalJS', 'nonCriticalJS', 'criticalCSS', 'nonCriticalCSS', 'watchTask', 'serve', 'nodeDev'));
