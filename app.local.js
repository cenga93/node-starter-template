import app from './app.js';
import Mongoose from 'mongoose';

/** Starting the server */
app.listen(process.env.PORT, async () => {
     const URL = process.env.DATABASE_URL;

     console.log(`
     --------------------------------------
        [PROJECT]  port::${process.env.PORT}
     --------------------------------------
     `);

     if (URL) {
          await Mongoose.connect(URL)
               .then(({ connections }) => {
                    console.log(`==> Connected to [${connections[0].name}] database`);
               })
               .catch((err) => {
                    console.log('Could not connect to the database. Exiting now...', err);
               });
     }

     console.log('==> Server is up');

     /** Error middleware */
});
