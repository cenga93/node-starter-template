# Started node template

### Base installation instruction

```shell
cd Desktop/
git clone https://gitlab.com/cenga93/node-starter-template.git
```

#### Open terminal into root project folder and run: `npm install`

---

_If you don't have and **node** and **npm**, execute the following command:_

#### This commands is for linux OS.

```shell
sudo apt update
sudo apt install nodejs
nodejs -v
sudo apt install npm
sudo npm install --global gulp gulp-cli
```

---

## Libraries in project

-

## Dependesies

-    compression
-    cors
-    dotenv
-    ejs
-    express
-    express-ejs-layouts
-    express-mongo-sanitize
-    fast-glob
-    mongoose
-    webp-converter


-    @babel/preset-env
-    babel-core
-    babel-eslint
-    babel-preset-env
-    babelify
-    browser-sync
-    browserify
-    eslint
-    eslint-config-airbnb-base
-    eslint-config-prettier
-    eslint-import-resolver-alias
-    eslint-plugin-import
-    eslint-plugin-prettier
-    gulp
-    gulp-file-include
-    gulp-htmlmin
-    gulp-nodemon
-    gulp-plumber
-    gulp-rename
-    gulp-sass
-    gulp-sass-lint
-    gulp-sassvg
-    gulp-size"
-    gulp-sourcemaps
-    gulp-string-replace
-    gulp-uglify
-    prettier
-    sass
-    vinyl-buffer
-    vinyl-source-stream
