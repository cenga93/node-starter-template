import dotenv from 'dotenv';
dotenv.config({ path: '.env' });
import express from 'express';
import expressLayouts from 'express-ejs-layouts';
import path from 'path';
import mongoSanitize from 'express-mongo-sanitize';
import compression from 'compression';
import cors from 'cors';
import mongoose from 'mongoose';
import { fileURLToPath } from 'url';
import router from './src/routes/index.js';

const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);

const __public = path.join(__dirname, 'public');
const __src = path.join(__dirname, 'src');
const __views = path.join(__src, 'views');

const app = express();

/** Sanitizes user-supplied data to prevent MongoDB Operator Injection. */
app.use(mongoSanitize());

/**  Compression middleware */
app.use(compression());

app.use(cors({ origin: (origin, callback) => callback(null, true) }));

app.use((req, res, next) => {
     res.header('Access-Control-Allow-Origin', '*');
     res.header('Access-Control-Allow-Methods', 'GET, PUT, POST, DELETE, OPTIONS , post, get');
     res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With,' + ' Content-Type, Accept');

     return next();
});

/** Parse application/x-www-form-urlencoded */
app.use(express.urlencoded({ extended: true }));

/** Layout */
app.use(expressLayouts);

/** Parse application/json */
app.use(express.json());

mongoose.Promise = global.Promise;

// Set template engine
app.set('view engine', 'ejs');
app.set('views', __views);
app.set('layout', 'layout/_default');

// Set static files
app.use('/css', express.static(path.join(__public, '/css')));
app.use('/js', express.static(path.join(__public, '/js')));
app.use('/images', express.static(path.join(__public, '/images')));
// app.use("/fonts", express.static(path.join(__public, "/fonts")));

/** Routes */
app.use('/', router());

app.use((req, res) => {
     res.status(404).json({
          message: '404',
     });
});

export default app;
