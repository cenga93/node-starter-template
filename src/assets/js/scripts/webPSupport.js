/**
 *  How to use webp image into HTML file:
 *
          <picture>
                    <source srcSet="<imagesPath>.webp" type="image/webp">
                    <img src="<imagesPath>.jpg" alt="insert alt text here">
          </picture>
 */

export default () => {
     const canvas = document.createElement('canvas');
     const body = document.querySelector('body');

     if (canvas.getContext && canvas.getContext('2d')) {
          body.classList.add('webp');

          return canvas.toDataURL('image/webp').indexOf('data:image/webp') === 0;
     }

     body.classList.add('no-webp');
     return false;
};
