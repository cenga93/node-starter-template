import { Router } from 'express';
import home from './home.js';

export default () => {
     const router = Router();

     router.use('/', home());

     return router;
};
