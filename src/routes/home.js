import { Router } from 'express';
import { home } from '../controllers/homeController.js';

export default () => {
     const router = Router();

     router.get('/', home);

     return router;
};
